  
/*!
 * Bootstrap有记录总数的分页插件 v1.0  
 * Author: dreling.mike@gmail.com
 * Licensed under the GPL 
 * Options: pageSize 每页记录 initPages 显示分页按纽个数 recordTotal 记录总数 showGoto 是否显示跳转 
 * Call: $("#div").dataPagerTotal({ pageChanged: function (pageIndex,pageSize) { //your code }});
 */
; (function ($) {
         
    var methods = {
        init: function (options) {
            return this.each(function () {

                var $this = $(this);
                
                var eid = $this[0].id;
                var settings = $this.data('settings'); 
                if (typeof (settings) == 'undefined') {
                    //settings = $.extend({}, $.fn.dataPager.defaults, options); //覆盖式
                    settings = $.extend($.fn.dataPagerTotal.defaults, options);
                    $this.data('settings', settings);
                } else {
                    settings = $.extend({}, settings, options);
                }
                 
                var maxpage = $this.data('maxPage');
                if (settings.recordTotal == 0 || !maxpage) {
                    $this.data('maxPage', settings.initPages);
                    maxpage = settings.initPages;
                } else {
                    maxpage = Math.ceil(settings.recordTotal / settings.pageSize);
                    if (settings.initPages > maxpage) {
                        settings.initPages = maxpage;
                    }
                }
                 
                $this.data('pageIndex', 1);
                $this.data('elementId', eid);
                  
                var btnhtml = '';
                for (var i = 1; i <= settings.initPages; i++) {
                    btnhtml = btnhtml + '<li  id="btn_page_' + i + eid + '" ><a href="#" onfocus="this.blur()">' + i + '</a></li> ';
                }

                var gohtml = '';
                if (settings.showGoto) {
                    gohtml = '<li><p style="display:inline;color:#999;font-size:12px;padding-left:5px;">共<span id="info_page_' + eid + '">' + maxpage +
                             '</span>页&nbsp;&nbsp;到第&nbsp;<input type="text" class="form-control input-sm data-pager-ipt" id="ipt_page_go_' + eid + '" >' +
                             '页&nbsp;<button type="button" class="btn btn-default btn-sm" id="btn_page_goto_' + eid + '" onfocus="this.blur()">确定</button></p></li>';
                }

                var html = '<div class="data-pager" id=""> <ul class="pagination"> ' +
                    '<li id="btn_page_first_' + eid + '" class="disabled"><a href="#" onfocus="this.blur()" >首页</a></li> ' +
                    '<li id="btn_page_prev_' + eid + '" class="disabled"><a href="#" onfocus="this.blur()" >上一页</a></li> ' + btnhtml +
                    '<li id="btn_page_next_' + eid + '" ><a href="#" onfocus="this.blur()" >下一页</a></li>  ' +
                    '<li id="btn_page_last_' + eid + '" ><a href="#" onfocus="this.blur()" >末页</a></li>' + gohtml + '</ul></div>';
               
                $this.html(html);

                $('#btn_page_first_' + eid).bind('click', { page: 1, that: $this }, pageChanged);
                $('#btn_page_prev_' + eid).bind('click', { page: "prev", that: $this }, pageChanged);
                $('#btn_page_next_' + eid).bind('click', { page: "next", that: $this }, pageChanged);
                $('#btn_page_last_' + eid).bind('click', { page: maxpage, that: $this }, pageChanged);
                //$('#ipt_page_go').bind('blur', { show: false }, showButton);
                //$('#ipt_page_go').bind('focus', { show: true }, showButton);

                if (settings.showGoto) $('#btn_page_goto_' + eid).bind('click', { page: "goto", that: $this }, pageChanged);
                if (settings.firstActive) $('#btn_page_1_' + eid).addClass("active");

                for (var j = 1; j <= settings.initPages; j++) { 
                    $('#btn_page_' + j + eid).bind('click', { that: $this }, pageChanged);
                }
                 
            });
        },
        destroy: function (options) {
            return $(this).each(function () {
                var $this = $(this);
                $this.removeData('settings');
                $this.removeData('pageIndex');
                $this.removeData('maxPage');
                $this.removeData('elementId');
            });
        },
        //设置记录总数
        recordTotal: function (count) {
            var $this = $(this); 
            var settings = $this.data('settings');
            var eid = $this.data('elementId');

            if (count == settings.recordTotal) return;
            var maxPage = Math.ceil(count / settings.pageSize);

            $('#info_page_' + eid).html(maxPage);

            $("#btn_page_last_" + eid).unbind("click");
            $('#btn_page_last_' + eid).bind('click', { page: maxPage, that: $this }, pageChanged);
             
            $this.data('maxPage', maxPage);
             
        },
        //重置插件
        reset: function () {

            var $this = $(this);
            var settings = $this.data('settings');
            var beforeIndex = $this.data('beforeIndex');
            var eid = $this.data('elementId');

            $('#btn_page_s1').hide();
            for (var i = settings.initPages + 1; i <= 6; i++) {
                $('#btn_page_' + i + eid).hide();
            }
            $('#btn_page_' + beforeIndex + eid).removeClass("active");
            $('#btn_page_1' + eid).addClass("active");

            $this.data('beforeIndex', 1);
        },
    };
   
    $.fn.dataPagerTotal = function (options) {
  
       
        var method = arguments[0];
        if (methods[method]) {
            method = methods[method]; 
            // 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if (typeof (method) == 'object' || !method) {
            method = methods.init;
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.pluginName');
            return this;
        }

        //用apply方法来调用我们的方法并传入参数
        return method.apply(this, arguments);  
         
    };

    $.fn.dataPagerTotal.defaults = {
        initPages: 5,
        pageSize: 20,
        recordTotal: 0,
        showGoto: true, 
    };
     
    function pageChanged(e) {
          
        if ($(e.currentTarget).hasClass('disabled') || $(e.currentTarget).hasClass('active')) return;

        var $this = e.data.that;
        var settings = $this.data('settings');
        var pageIndex = $this.data('pageIndex');
        var maxPage = $this.data('maxPage');
        if (!maxPage) maxPage = 0;
        var eid = $this.data('elementId');
        var index = 0;

        if (e.data.page != null) {
            var para = e.data.page;
            if (para == "prev") {
                if (pageIndex > 1) {
                    index = pageIndex - 1;
                } else {
                    index = 1;
                }
            } else if (para == "next") {
                if (pageIndex < maxPage || maxPage == 0) {
                    index = pageIndex + 1;
                }
                else {
                    index = maxPage;
                }
            } else if (para == "goto") {    //goto
                var page = $('#ipt_page_go_' + eid).val();
                if (!(/(^[1-9]\d*$)/.test(page))) return;    //不是大于0的整数 
                page = parseInt(page);
                if (maxPage != 0 && page > maxPage) {
                    page = maxPage;
                    $('#ipt_page_go_' + eid).val(maxPage);
                }
                index = page;
            }
            else {
                index = para;
            }
        } else {
            index = parseInt(e.target.text);
        }

        renderNumber(index, $this);
        renderEnable(index, $this); 
        $this.data('pageIndex', index);

        if (settings.pageChanged !== undefined) {
            settings.pageChanged(index, settings.pageSize);
        }

    };

    function showButton(e) {

        var $this = e.data.that;
        var isshow = e.data.show;
        var eid = $this.data("elementId");

        if (isshow) { 
            $("#btn_page_goto_" + eid).show(500);
        } else {
            $("#btn_page_goto_" + eid).hide(500);
        }
        
    };

    function renderNumber(index,that) {

        var $this = that;
        var show = that.data('settings').initPages;
        var total = that.data('maxPage');
        var beforeindex = that.data('beforeIndex');
        var eid = that.data('elementId');

        var start = 1;
        var position = 1;

        var flag = Math.ceil(show / 2);
        if (index > flag) { //超过一半
            start = index - flag + 1;
        }
        if (start > total - show + 1) {  //末尾了
            start = total - show + 1;
        }
        if (start <= 0) start = 1;

        for (var i = start; i <= start + show - 1; i++) {
            var btnid = i - start + 1;
            $('#btn_page_' + btnid + eid).find("a").html(i);
            if (index == i) position = btnid;
        }

        //按纽样式
        if (beforeindex == 0) {
            $('#btn_page_' + position + eid).addClass("active");
            $('#btn_page_1' + eid).removeClass("active");
        } else if (position != beforeindex) {
            $('#btn_page_' + position + eid).addClass("active");
            $('#btn_page_' + beforeindex + eid).removeClass("active");
        }
     
        $this.data('beforeIndex', position);
    };

    function renderEnable(index, that) {
         
        var maxpage = that.data('maxPage');
        var eid = that.data('elementId');

        var pstate = $("#btn_page_prev_" + eid).hasClass('disabled');
        var nstate = $("#btn_page_next_" + eid).hasClass('disabled');

        if (index == 1 && !pstate) {
            $("#btn_page_prev_" + eid).addClass("disabled");
            $("#btn_page_first_" + eid).addClass("disabled");
        } else if (pstate) {
            $("#btn_page_prev_" + eid).removeClass("disabled");
            $("#btn_page_first_" + eid).removeClass("disabled");
        }

        if (index == maxpage ) {
            $("#btn_page_next_" + eid).addClass("disabled");
            $("#btn_page_last_" + eid).addClass("disabled");
        } else if (nstate) {
            $("#btn_page_next_" + eid).removeClass("disabled");
            $("#btn_page_last_" + eid).removeClass("disabled");
        }
    };
  
})(jQuery);
 
 
/*!
 * Bootstrap风格仿淘宝不计记录总数的分页插件 v1.0  
 * Author dreling.mike@gmail.com
 * Licensed under the GPL 
 * Options: pageSize 每页记录 initPages 首次加截显示的按纽数 showGoto 是否显示跳转 
 * Call: $("#div").dataPagerTotal({ pageChanged: function (pageIndex,pageSize) { //your code }});
 */
; (function ($) {
      
    var methods = { 
        init: function (options) { 
            return this.each(function () {
                 
                var dataPager = $(this);
                var settings = dataPager.data('dataPager');
                var eid = dataPager[0].id;

                if (typeof (settings) == 'undefined') {  
                    //settings = $.extend({}, $.fn.dataPager.defaults, options);
                    settings = $.extend( $.fn.dataPager.defaults, options);
                    dataPager.data('dataPager', settings);
                } else {
                    settings = $.extend({}, settings, options);
                }
 
                dataPager.data('pageIndex', 1);
                dataPager.data('elementId', eid);

                var fistavtive = '';
                if (settings.firstActive) fistavtive = ' class="active"';

                var btnhtml =
                    '<li id="btn_page_1' + eid + '" ' + fistavtive + '><a href="#" onfocus="this.blur()" >1</a></li> ' +
                    '<li id="btn_page_2' + eid + '" ><a href="#" onfocus="this.blur()" >2</a></li> ' +
                    '<li id="btn_page_s1' + eid + '" class="disabled"><a onfocus="this.blur()" class="disabled">...</a></li>' +
                    '<li id="btn_page_3' + eid + '" ><a href="#" onfocus="this.blur()" >3</a></li> ' +
                    '<li id="btn_page_4' + eid + '" ><a href="#" onfocus="this.blur()" >4</a></li>' +
                    '<li id="btn_page_5' + eid + '" ><a href="#" onfocus="this.blur()" >5</a></li>' +
                    '<li id="btn_page_6' + eid + '" ><a href="#" onfocus="this.blur()" >6</a></li>' +
                    '<li id="btn_page_s2' + eid + '" class="disabled"><a href="#" onfocus="this.blur()">...</a></li>';

                var gohtml = '';
                if (settings.showGoto) {
                    gohtml = '<li>&nbsp;&nbsp;到第<input type="text" class="form-control data-pager-ipt" id="ipt_page_go' + eid + '" >页' +
                             '<button type="button" class="btn btn-default" id="btn_page_goto' + eid + '" onfocus="this.blur()">确定</button></li>';
                }

                var html = '<div class="data-pager"> <ul class="pagination"> ' +
                    '<li id="btn_page_prev' + eid + '" class="disabled"><a href="#" onfocus="this.blur()" >上一页</a></li> ' + btnhtml +
                    '<li id="btn_page_next' + eid + '" ><a href="#" onfocus="this.blur()" >下一页</a></li>' + gohtml + '</ul></div> ';
              

                dataPager.html(html);

                $('#btn_page_prev' + eid).bind('click', { page: "prev", that: dataPager }, pageChanged);
                $('#btn_page_next' + eid).bind('click', { page: "next", that: dataPager }, pageChanged);
                $('#btn_page_goto' + eid).bind('click', { page: "goto", that: dataPager }, pageChanged);

                for (var j = 1; j <= 6; j++) {
                    $('#btn_page_' + j + eid).bind('click', { that: dataPager }, pageChanged);
                }
                $('#btn_page_s1' + eid).hide();

                for (var i = settings.initPages + 1 ; i <= 6; i++) {
                    $('#btn_page_' + i + eid).hide();
                }

            });
        }, 
        destroy: function (options) {
            return $(this).each(function () {
                var dataPager = $(this);
                dataPager.removeData('dataPager');
                dataPager.removeData('pageIndex');
                dataPager.removeData('beforeIndex');
                dataPager.removeData('maxPage');
                dataPager.removeData('elementId');
            });
        }, 
        //设置最大页码数
        maxPage: function (count) {
            var dataPager = $(this);
            dataPager.data('maxPage', count);

           
            var pageIndex = dataPager.data('pageIndex');
            var eid = dataPager.data('elementId');
            var position;

            if (pageIndex >= count) {
                if (pageIndex < 6) {
                    position = pageIndex;
                } else {
                    position = 5;
                } 
                for (var i = position + 1; i <= 6; i++) {
                    $('#btn_page_' + i + eid).hide(); 
                }
                $('#btn_page_s2' + eid).hide();
                $("#btn_page_next" + eid).addClass("disabled");
            } else if (pageIndex > count ) {
                for (var j =  1; j <= 6; j++) {
                    $('#btn_page_' + j + eid).hide();
                    $('#btn_page_s2' + eid).hide();
                }
            }
        }, 
        //重置插件
        reset: function () {

            var dataPager = $(this); 
            var options = dataPager.data('dataPager');
            var eid = dataPager.data('elementId'); 
            for (var i = options.initPages + 1; i <= 6; i++) {
                $('#btn_page_' + i + eid).hide();
            } 
            var beforeIndex = dataPager.data('beforeIndex');
            $('#btn_page_' + beforeIndex + eid).removeClass("active");
             $('#btn_page_1' + eid).addClass("active");
             
            $("#btn_page_prev" + eid).addClass("disabled");
            $("#btn_page_next" + eid).removeClass("disabled");
            dataPager.data('beforeIndex', 1);
        } ,
    };
  
    var pageChanged = function (e) {
       
        if ($(e.currentTarget).hasClass('disabled') || $(e.currentTarget).hasClass('active')) {
            return;
        }

        var that = e.data.that;

        var pageIndex = that.data('pageIndex');
        var option = that.data('dataPager');
        var eid = that.data('elementId');

        if (!pageIndex) pageIndex = 1;
        var maxPage = that.data('maxPage');
        if (!maxPage) maxPage = 0;

        var index = 1;
        if (e.data.page != null) {
            var para = e.data.page;
            if (para == "prev") {
                if (pageIndex > 1) {
                    index = pageIndex - 1;
                } else {
                    index = 1;
                }
            } else if (para == "next") {
                if (pageIndex < 100) {
                    index = pageIndex + 1;
                }
                else {
                    index = 100;
                }
            } else {    //goto
                var page = $('#ipt_page_go' + eid).val();
                if (!(/(^[1-9]\d*$)/.test(page))) return;    //不是大于0的整数 
                page = parseInt(page);
                if (maxPage != 0 && page > maxPage) {
                    page = maxPage;
                    $('#ipt_page_go' + eid).val(maxPage);
                }
                index = page;
            }
        } else {
            index = parseInt(e.target.text);
        }

        that.data('pageIndex', index);
         
        renderNumber(index, that);
        renderEnable(index, that);
         
        if (option.pageChanged !== undefined) {
            option.pageChanged(index, option.pageSize);
        }

    };

    //重新设置分页按纽的页码
    var renderNumber = function (index, that) {
         
        var option = that.data('dataPager');
        var eid = that.data('elementId');

        for (var s = option.initPages + 1; s <= 6; s++) {
            $('#btn_page_' + s + eid).hide();
        }
        $('#btn_page_s1' + eid).hide(); 
        $('#btn_page_s2' + eid).show();

        if (index >= 6) $('#btn_page_s1' + eid).show();
        if (index >= 2) $('#btn_page_3' + eid).show();
        if (index >= 3) $('#btn_page_4' + eid).show();
        if (index >= 4) $('#btn_page_5' + eid).show();
        if (index >= 5) $('#btn_page_6' + eid).show();

        var start = 1;
        var position = 1;

        if (index < 6) {
            start = 3;
            position = index;
        } else {
            start = index - 2;
        }

        for (var i = start; i <= start + 3; i++) {
            var btnid = i - start + 3;
            $('#btn_page_' + btnid + eid).find("a").html(i);
            if (index == i) position = btnid;
        }

        var beforeIndex = that.data('beforeIndex');
        //按纽样式
        if (!beforeIndex || beforeIndex == 0) {

            $('#btn_page_1' + eid).removeClass("active");
            $('#btn_page_' + position + eid).addClass("active");
        } else if (position != beforeIndex) {

            $('#btn_page_' + beforeIndex + eid).removeClass("active");
            $('#btn_page_' + position + eid).addClass("active");
        }

        that.data('beforeIndex', position);
    };

    //上下一页按纽可用
    var renderEnable = function (index, that) {

        var maxPage = that.data('maxPage');
        var eid = that.data('elementId');

        var pstate = $("#btn_page_prev" + eid).hasClass('disabled');
        var nstate = $("#btn_page_next" + eid).hasClass('disabled');
       
        if (index == 1) {
            $("#btn_page_prev" + eid).addClass("disabled");
        } else if (pstate) {
            $("#btn_page_prev" + eid).removeClass("disabled");
        }
        if (index == maxPage && maxPage != 0) {
            $("#btn_page_next" + eid).addClass("disabled");
        } else if (nstate) {
            $("#btn_page_next" + eid).removeClass("disabled");
        }
    };

    $.fn.dataPager = function (options) {
         
        var method = arguments[0]; 
        if (methods[method]) {
            method = methods[method];

            // 我们的方法是作为参数传入的，把它从参数列表中删除，因为调用方法时并不需要它
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if (typeof (method) == 'object' || !method) {
            method = methods.init;
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.pluginName');
            return this;
        }

        ////用apply方法来调用我们的方法并传入参数
        return method.apply(this, arguments); 
    };

    //默认配置
    $.fn.dataPager.defaults = {
        pageSize: 20,
        initPages: 5,
        showGoto: false,
        firstActive:false,
    };
      
})(jQuery);
 